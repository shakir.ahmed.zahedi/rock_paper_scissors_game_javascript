
async function getToken() {
    const response = await fetch('https://java19.sensera.se/auth/token');
    const data = await response.text();
    return data;
}

async function getGameById(token, gameId) {
    const response = await fetch('https://java19.sensera.se/games/join/' + gameId, { headers: { 'token': token } });
    const result = await response.json();
    return result;
}

async function getAllGames(token) {
    const response = await fetch('https://java19.sensera.se/games', { headers: { 'token':token} });
    const result = await response.json();
    return result;
}

async function setName(token,data = {}) {
    const response = await fetch('https://java19.sensera.se/user/name', 
    {method: 'POST',
     headers: {  "Content-type": "application/json",'token':token}, 
     body:JSON.stringify(data)});
    const result = await response.text();
    return result;
}

async function getStartGame(token) {
    const response = await fetch('https://java19.sensera.se/games/start',{ headers: { 'token':token} });
    const result =response.json();
    return result;
}

async function getGameStatus(token) {
    const response = await fetch('https://java19.sensera.se/games/status',{ headers: { 'token':token} });
    const result =response.json();
    return result;
}

async function getGameInfo(token,gameId) {
    const response = await fetch('https://java19.sensera.se/games/'+gameId,{ headers: { 'token':token} });
    const result =response.json();
    return result;
}

async function getMove(token,sign) {
    const response = await fetch('https://java19.sensera.se/games/move/'+sign,{ headers: { 'token':token} });
    const result =response.json();
    return result;
}










