localStorage.setItem("status", true);
const res = sessionStorage.getItem("myToken");
const moveRegistrationLabel = document.querySelector("#moveRegistrationLabel");

function checkGameStatus(res) {

    getGameStatus(res).then(data => {
        console.log(data);
        if (data.opponentMove === "" || data.opponentMove === null) {
            // display game option
            console.log(data.move);

        }
        else {
            const gameResult = data.game;
            updateGameResult(gameResult);
            updatePlayerMove(data.move);
            updateOpponentMove(data.opponentMove)
            moveRegistrationLabel.innerHTML = "The game Result is published";

        }
    });
}

function handelPlayerMove(playerMove) {
    const status = localStorage.getItem("status");

    if (status === true || status === 'true') {
        getMove(res, playerMove).then(data => {
            console.log(data);
            alert("Move Is successfully registered");
            localStorage.setItem("currentId", data.gameId);

            moveRegistrationLabel.innerHTML = "Your move is registerded and waiting for your opponent";
        }).finally(() => {
            window.setInterval(function () {
                /// call your function here
                checkGameStatus(res);//(localStorage.getItem("currentId"));
            }, 1000);
        });
        localStorage.setItem("status", false);
    }
    else
        alert("Move can be registered once!!")

}
