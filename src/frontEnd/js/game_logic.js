const ROCK='ROCK';
const PAPER='PAPER';
const SCISSORS='SCISSORS';
const MOVE=['ROCK','PAPER','SCISSORS'];
const TIE = 'TIE';
const WIN = 'WIN';
const LOSE = 'LOSE';
var playerScore = 0;
var computerScore = 0;

function decideWinner(playerMove, computerMove){
    if(playerMove===computerMove){
        return 'TIE';
    }
    switch (playerMove) {
     case ROCK: return computerMove === SCISSORS ? WIN : LOSE;
     case PAPER: return computerMove === ROCK ? WIN : LOSE;
     case SCISSORS: return  computerMove === PAPER ? WIN : LOSE;
 }
 throw new Error('Unknown playerMove',playerMove);


}

function decideWinner_with_human(playerMove, opponentMove) {
    if (playerMove === opponentMove) {
        return 'TIE';
    }
    switch (playerMove) {
        case ROCK: return opponentMove === SCISSORS ? WIN : LOSE;
        case PAPER: return opponentMove === ROCK ? WIN : LOSE;
        case SCISSORS: return opponentMove === PAPER ? WIN : LOSE;
    }
    throw new Error('Unknown playerMove', playerMove);


}

function CalculatecomputerMove(){
    let randomlySelectComputerMove=Math.floor(Math.random()*3);
    return MOVE[randomlySelectComputerMove];
    
}

function updateGameScore(gameResult) {
 if (gameResult === WIN) {
     playerScore++;
     updatePlayerScore(playerScore);
 }
 if (gameResult === LOSE) {
     computerScore++;
    updateComputerScore(computerScore);
 }
}