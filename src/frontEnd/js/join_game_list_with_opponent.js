const listRow=document.querySelector('.listRow');
const res= sessionStorage.getItem("myToken");
const playerName=sessionStorage.getItem("playerName");
console.log(playerName);

joinGameFromTheList(res);

function joinGameFromTheList(res){
        getAllGames(res).then(games=>{
           const newGames= games.filter(game=>game.name != playerName);
            updateJoinGameListPage(newGames);
        })
}

function goToGamePageWithHuman(gameId) {
    const button = document.querySelector('.button1');
           
        getGameById(res,gameId).then(data=>{
                console.log(data);
               localStorage.setItem("gameId",data.gameId);
               window.location.assign('./game_page_with_opponent_for_result.html');
                
            });
}



function updateJoinGameListPage(games) {
   var sortedGames = games.sort((a,b)=> {
       if(a.name===null || a.name==="null"){
           a.name="Unknown";
       }
       if(b.name===null || b.name==="null"){
        b.name="Unknown";
    }
        var a1 = a.name.toLowerCase();
        var b1 = b.name.toLowerCase();
        return a1<b1 ?-1:a1> b1? 1 :0;
        });
        sortedGames.map((game, i) => {
        const singleColumn = document.createElement('div');
        singleColumn.classList.add("col-10");
        singleColumn.classList.add("col-sm-5");
        singleColumn.classList.add("col-md-3");
        singleColumn.classList.add("justify-content-between");
        
        singleColumn.classList.add("card");
        singleColumn.classList.add("m-1");
        singleColumn.classList.add("px-0");
        singleColumn.classList.add("text-center");

        const singleColumnTitel = document.createElement('div');
        if(i%2===0){
            singleColumnTitel.classList.add("bg-danger");
        }
        else{
            singleColumnTitel.classList.add("bg-pink");
        }

        singleColumnTitel.classList.add("card-titel");
        singleColumnTitel.classList.add("text-light");
        singleColumnTitel.classList.add("m-0");
        singleColumnTitel.innerHTML = '<h5> Player: ' + game.name + '</h5>';

        const singleColumnBody = document.createElement('div');
        singleColumnBody.classList.add("card-body");
        singleColumnBody.classList.add("bg-light");
        singleColumnBody.innerHTML = '<p> GameId:' + game.gameId + '</p>';

        const singleColumnFooter = document.createElement('div');
        singleColumnFooter.classList.add("card-footer");
        singleColumnFooter.classList.add("bg-info");
        const button = document.createElement('button');
        button.classList.add("button1");
        button.classList.add("btn");
        button.classList.add("btn-light");
        button.setAttribute("onclick", "goToGamePageWithHuman('" + game.gameId + "')");
        button.setAttribute("value", game.game)
        button.innerText = "Join";

        singleColumnFooter.appendChild(button);

        singleColumn.appendChild(singleColumnTitel);
        singleColumn.appendChild(singleColumnBody);
        singleColumn.appendChild(singleColumnFooter);
        listRow.appendChild(singleColumn);

    })

}

