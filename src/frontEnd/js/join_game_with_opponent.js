const connectButton = document.querySelector('#connectButton');


function goToGameListPage() {
  const playerName = document.querySelector("#playerName").value;
  const unKnownPlayer = "UNKNOWN";
  const exitPlayerName = sessionStorage.getItem("playerName");
  console.log(playerName);
  console.log(exitPlayerName);

  if (exitPlayerName === "" || exitPlayerName === null || exitPlayerName === "null") {
    sessionStorage.setItem("playerName", playerName);
    console.log(playerName);
    console.log(exitPlayerName);
  }
  else {
    sessionStorage.setItem("playerName", exitPlayerName);
    console.log(playerName);
    console.log(exitPlayerName);
  }

  console.log(sessionStorage.getItem("playerName"));
  //fix the token and save
  checkToken(sessionStorage.getItem("playerName"));



}

function checkToken(name) {
  const token = sessionStorage.getItem("myToken");
  if (token === "" || token === null || token === "null") {
    getToken().then(res => {
      sessionStorage.setItem("myToken", res);
      setName(res, { 'name': name }).then(data => {
        console.log(data);
      }).finally(() => {
        window.location.assign('./join_game_list_page_with_opponent.html');
      });
    });

  }
  else {
    window.location.assign('./join_game_list_page_with_opponent.html');

  }
}