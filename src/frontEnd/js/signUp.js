document.addEventListener('DOMContentLoaded', function(){
   
    console.log("aaaaa");
   
    
 });

function formSubmittion(){
        formValidation();

    }


function formValidation() {

   

    const lastName = document.getElementById('lastName');
    const userEmail = document.getElementById('userEmail');
    const password = document.getElementById('password');
    const confirmPassword = document.getElementById('confirmPassword');
    const firstName = document.querySelector('#firstName');
    
	// trim to remove the whitespaces
    const firstNameValue = firstName.value.trim();
    const lastNameValue = lastName.value.trim();
	const userEmailValue = userEmail.value.trim();
	const passwordValue = password.value.trim();
    const confirmPasswordValue = confirmPassword.value.trim();
    console.log(firstNameValue.length);

    if(!validateName(firstNameValue)|| firstNameValue.length > 12){
        setErrorFor(firstName, 'Please enter valid Name');
    }
    else{
        setSuccessFor(firstName);
    }

    if(!validateName(lastNameValue) || lastNameValue.length > 12){
        setErrorFor(lastName, 'Please enter valid Name');
    }
    else{
        setSuccessFor(lastName);
    }

    if(!validateEmail(userEmailValue)){
        setErrorFor(userEmail, 'Please enter valid Email');
    }
    else{
        setSuccessFor(userEmail);
    }
    if(!validatePassword(passwordValue)){
        setErrorFor(password, 'Please enter valid Password');
    }
    else{
        setSuccessFor(password);
    }

    if(!validatePassword(confirmPasswordValue)|| passwordValue!=confirmPasswordValue){
        setErrorFor(confirmPassword, 'Please re-enter  Password ');
    }
    else{
        setSuccessFor(confirmPassword);
    }
    
	
}


function validateEmail(email) {
    const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailReg.test(email);
}

function validateName(name){
    const nameReg=/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;
    return nameReg.test(name);

}

function validatePassword(password){
    const passwordReg=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return passwordReg.test(password);
}

function setErrorFor(input, message) {
    const formControl = input.parentElement;
    const ico=formControl.querySelector("span .fa-exclamation-circle");
    ico.style.visibility="visible";
    ico.style.color="red";
    const small = formControl.querySelector('small');
    input.style.border="1px solid red";
	
    small.innerText = message;
    small.style.visibility="visible";
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	const ico=formControl.querySelector("span .fa-check-circle");
    ico.style.visibility="visible";
    ico.style.color="green";
    const small = formControl.querySelector('small');
    input.style.border="1px solid green";
}