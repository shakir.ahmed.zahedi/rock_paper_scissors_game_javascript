
function updateComputerMove(computerMove){
    const computerMoveElement=document.querySelector('#computerMove');
    computerMoveElement.innerHTML=computerMove;
}
function updateComputerScore(computerScore){
    const computerScoreElement=document.querySelector('#computerScore')
    computerScoreElement.innerHTML=computerScore;
}

function updatePlayerMove(playerMove){
    const playerMoveElement=document.querySelector('#playerMove');
    playerMoveElement.innerHTML=playerMove;
}

function updatePlayerScore(playerScore){
    const playerScoreElement=document.querySelector('#playerScore');
    playerScoreElement.innerHTML=playerScore;
    
}

function updateGameResult(gameResult){
    const resultElement=document.querySelector('#gameResult');
    resultElement.innerHTML=gameResult;
}
