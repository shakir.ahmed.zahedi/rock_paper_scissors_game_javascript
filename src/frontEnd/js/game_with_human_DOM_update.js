function updateGameResult(gameResult) {
    const resultElement = document.querySelector('#gameResult');
    resultElement.innerHTML = gameResult;
}
function updatePlayerMove(playerMove) {
    const playerMoveElement = document.querySelector('#playerMove');
    playerMoveElement.innerHTML = playerMove;
}
function updateOpponentMove(opponentMove) {
    const opponentMoveElement = document.querySelector('#opponentMove');
    opponentMoveElement.innerHTML = opponentMove;
}