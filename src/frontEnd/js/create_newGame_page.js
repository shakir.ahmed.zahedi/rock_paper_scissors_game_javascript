const newGameJoinButton = document.querySelector("#newGameJoinButton");
const radioButton = document.querySelectorAll("input[name]");

function creatNewGame() {
  const playerName = document.querySelector("#playerName").value;
  const exitPlayerName = sessionStorage.getItem("playerName");

  if (exitPlayerName === "" || exitPlayerName === null || exitPlayerName === "null") {
    sessionStorage.setItem("playerName", playerName);
  }
  else {
    sessionStorage.setItem("playerName", exitPlayerName);
  }


  if (radioButton[0].checked === true) {
    //
    window.location.assign('./game_page_with_computer.html');
  }
  else {
    checkTokenToCreateNewGame(sessionStorage.getItem("playerName"));
  }
}


function checkTokenToCreateNewGame(name) {
  const token = sessionStorage.getItem("myToken");
  if (token === "" || token === null || token === "null") {
    getToken().then(res => {
      sessionStorage.setItem("myToken", res);
      setName(res, { 'name': name }).then(data => {
        console.log(data);
      });
      getStartGame(res).then(data => {
        console.log(data);

      }).finally(() => {
        window.location.assign('./game_page_with_opponent_to_register_move.html');
      });
    });

  }
  else {
    getStartGame(token).then(data => {
      console.log(data);
    }).finally(() => {
      window.location.assign('./game_page_with_opponent_to_register_move.html');
    });
  }
}